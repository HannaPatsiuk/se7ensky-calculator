window.controller = new ScrollMagic.Controller();
var langs = ['ua', 'en', 'de'];


(function(){

}());

var usdCheck;
var uahCheck;
var interestCalculatedUSD;
var interestCalculatedUAH;
var usd = document.getElementById('usd');
var uah = document.getElementById('uah');
var monthly = document.getElementById('month');
var once = document.getElementById('once');
var term = document.getElementById('slider2').value;
var value = document.getElementById('slider1').value;

//setting range value on page load

document.addEventListener('DOMContentLoaded', function(){   
  setValue(10000);
  setTerm(3);
  showValue();
})

function setValue(val) {
	document.getElementById('slider1').value = val;
}
function setTerm(term) {
	document.getElementById('slider2').value = term;
}
function showValue(){
	var value = document.getElementById('slider1').value;
	var term = document.getElementById('slider2').value;
	document.getElementById('val1').innerHTML = "$"+value;
	document.getElementById('term1').innerHTML = term+" мес";

}

// changing currency + capitalization on button click

var buttons_currency = document.getElementsByClassName('white_currency');
for (var i = buttons_currency.length - 1; i >= 0; i--) {
	buttons_currency[i].addEventListener("click", classUpdate());
}

var buttons_capitalization = document.getElementsByClassName("white_term");
for (var i = buttons_capitalization.length - 1; i >= 0; i--) {
buttons_capitalization[i].addEventListener("click", termUpdate());
}


function classUpdate(){
	usd.classList.toggle('active');
	uah.classList.toggle('active');
}

function termUpdate(){
	monthly.classList.toggle('active');
	once.classList.toggle('active');
}


// check what currenct is set

if(usd.classList.contains('active')){
	usdCheck = true;
}else{
	uahCheck = true;
}
// determine interest rate

if(month.classList.contains('active')){
monthlyCheck = true;
}else{
onceCheck = true;
}

document.getElementById("slider2").oninput = calculateSumChange();

function calculateSumChange(){
	var interest = document.getElementById('interest_rate');
	if(monthlyCheck){
		if (usdCheck) {
			if (term<6) {
				interestCalculatedUSD=12;
				interest.innerHTML = interestCalculatedUSD+"%"
			}
			else if(term>=6 && term<12){
				interestCalculatedUSD=16;
				interest.innerHTML = interestCalculatedUSD+"%"

			}
			else if(term>=12&&term<24){
				interestCalculatedUSD=18;
				interest.innerHTML = interestCalculatedUSD+"%"

			}
			else if(term>=24&&term<36){
				interestCalculatedUSD=19;
				interest.innerHTML = interestCalculatedUSD+"%"

			}
			else {interestCalculatedUSD=20;
			interest.innerHTML = interestCalculatedUSD+"%"

			}
		}
		else{
			if (term<6) {
				interestCalculatedUAH=21;
				interest.innerHTML = interestCalculatedUSD+"%"
			}
			else if(term>=6 && term<12){
				interestCalculatedUAH=23;
				interest.innerHTML = interestCalculatedUSD+"%"

			}
			else if(term>=12&&term<24){
				interestCalculatedUAH=25;
				interest.innerHTML = interestCalculatedUSD+"%"

			}
			else if(term>=24&&term<36){
				interestCalculatedUAH=28;
				interest.innerHTML = interestCalculatedUSD+"%"

			}
			else {interestCalculatedUAH=31;
			interest.innerHTML = interestCalculatedUAH+"%"
			}	
		}		
	}
	else{
		if (usdCheck) {
			if (term<6) {
				interestCalculatedUSD=13;
				interest.innerHTML = interestCalculatedUSD+"%"
			}
			else if(term>=6 && term<12){
				interestCalculatedUSD=17;
				interest.innerHTML = interestCalculatedUSD+"%"

			}
			else if(term>=12&&term<24){
				interestCalculatedUSD=20;
				interest.innerHTML = interestCalculatedUSD+"%"

			}
			else if(term>=24&&term<36){
				interestCalculatedUSD=21;
				interest.innerHTML = interestCalculatedUSD+"%"

			}
			else {interestCalculatedUSD=22;
			interest.innerHTML = interestCalculatedUSD+"%"

			}
		}
		else{
			if (term<6) {
				interestCalculatedUAH=22;
				interest.innerHTML = interestCalculatedUSD+"%"
			}
			else if(term>=6 && term<12){
				interestCalculatedUAH=24;
				interest.innerHTML = interestCalculatedUSD+"%"

			}
			else if(term>=12&&term<24){
				interestCalculatedUAH=27;
				interest.innerHTML = interestCalculatedUSD+"%"

			}
			else if(term>=24&&term<36){
				interestCalculatedUAH=30;
				interest.innerHTML = interestCalculatedUSD+"%"

			}
			else {interestCalculatedUAH=33;
			interest.innerHTML = interestCalculatedUAH+"%"
			}	
		}
	}

};

// calculate passive income per month

function calculatePassiveIncome(){

var incomeSum = document.getElementById('incomeTotal');
var incomeSumMonth = document.getElementById('incomeMonthly');
if(monthlyCheck){
	if (usdCheck) {
		var incomeTotal = value*(interestCalculatedUSD/100/12)*term;
		incomeSum.innerHTML = "$"+incomeTotal;
		var incomeMonthly = incomeTotal/term;
		incomeSumMonth.innerHTML = "$"+incomeMonthly;

	}
	else{
		var incomeTotal = value*(interestCalculatedUAH/100/12)*term;
		incomeSum.innerHTML = incomeTotal+"uah";
		var incomeMonthly = incomeTotal/term;
		incomeSumMonth.innerHTML = incomeMonthly+"uah";
		}	
	}
else{
	if (usdCheck) {
		var newInterest = Math.pow((1+interestCalculatedUSD/100/12), term);
		var incomeTotal = value*newInterest-value;
		incomeSum.innerHTML = "$"+incomeTotal;
		var incomeMonthly = incomeTotal/term;
		incomeSumMonth.innerHTML = "$"+incomeMonthly;

	}
	else{
		var incomeTotal = value*(interestCalculatedUAH/100/12)*term;
		incomeSum.innerHTML = incomeTotal+'uah';
		var incomeMonthly = incomeTotal/term;
		incomeSumMonth.innerHTML = incomeMonthly+'uah';
		}
	}
}
calculatePassiveIncome();

